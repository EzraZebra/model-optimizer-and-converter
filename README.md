# Model Optimizer and Converter
 - Optimizes WC3 Model files
 - Converts between MDL and MDX files (WC3 models).  
 
Uses [MDLX Parser library](https://gitlab.com/EzraZebra/mdlx-parser).

Download on [Hiveworkshop](https://www.hiveworkshop.com/threads/323265/).