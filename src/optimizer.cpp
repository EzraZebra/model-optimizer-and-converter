#include "util.h"
#include "options.h"
#include "optimizer.h"

#include <model.h>
#include <animation.h>
#include <animation_edit.h>
#include <animationmap.h>
#include <layer_edit.h>
#include <texture_edit.h>

#include <cmath>

void Optimizer::round(float &val)
{
    val = QString::number(val, 'f', precision).toFloat();
}

Optimizer::Optimizer(mdlx::Model &model, uint32_t options, uint32_t sanOptions, int precision)
    : options(options),
      sanOptions(sanOptions),
      precision(precision),
      model(model)
{
    try {
        model.prepareEdit();
    }
    catch(const std::exception &e) {
        emit status("Failed to edit model: " + QString::fromStdString(e.what()), Msg::Error);
    }
}

void Optimizer::optimize()
{
    // Sequences & Animations
    if (options & Opt::SEQS::ZeroLength) deleteZeroLengthSequences();
    if (options & Opt::SEQS::ZeroLengthGlob) deleteZeroLengthGlobalSequences();
    if (options & Opt::SEQS::DeleteUnused || options & Opt::SEQS::DeleteEmpty || Opt::SEQS::DeleteSameish
            || options & Opt::SEQS::Linearize || (options & Opt::Precision && precision >= 0))
    {
        const auto animatedObjects = model.edit.getAnimatedObjects();
        const auto animations = model.edit.getAnimations(animatedObjects);
        if (options & Opt::SEQS::DeleteUnused) deleteUnusedKeyframes(animations);
        if (options & Opt::SEQS::DeleteSameish) deleteSameishKeyframes(animations);
        if (options & Opt::SEQS::DeleteEmpty) deleteEmptyAnimations(animatedObjects);
        if (options & Opt::SEQS::Linearize) linearizeAnimations(animations);

        // Round to precision
        if (options & Opt::Precision && precision >= 0) {
            doRounding(animations);
        }
    }

    // Misc
    if (options & Opt::MISC::UnknownChunksDelete) {
        model.deleteUnknownChunks();
    }

    uint32_t oldDiffCount, pass = 1;
    do {
        oldDiffCount = diffCount;

        emit status("Pass " + QString::number(pass++) + "...", Msg::Notice);

        // Materials & Textures
        if (options & Opt::MTLS::Delete) deleteUnusedMaterials();
        if (options & Opt::MTLS::Merge) mergeIdenticalMaterials();
        if (options & Opt::TEXS::Delete) deleteUnusedTextures();
        if (options & Opt::TEXS::Merge) mergeIdenticalTextures();
        if (options & Opt::TXAN::Delete) deleteUnusedTextureAnimations();
        if (options & Opt::TXAN::Merge) mergeIdenticalTextureAnimations();

        // Geosets
        if (options & Opt::GEOS::Tris) deleteFakeTriangles();
        if (options & Opt::GEOS::VtxDelete) deleteFreeVertices();
        if (options & Opt::GEOS::VtxMerge) mergeIdenticalVertices();
        if (options & Opt::GEOS::MtxGrpDelete) deleteUnusedMatrixGroups();
        if (options & Opt::GEOS::SeqExt) deleteSequenceExtents();
        if (options & Opt::GEOS::Merge) mergeSimilarGeosets();
        if (options & Opt::GEOS::GeoAnimDelete) deleteUnusedGeosetAnimations();
        if (options & Opt::GEOS::GeoAnimDuplDelete) deleteDuplicateGeosetAnimations();

        // Nodes
        if (options & Opt::NODS::BoneHelperDelete) deleteUnusedBonesHelpers();
        if (options & Opt::NODS::EventObjDelete) deleteUnusedEventObjects();

        // Misc
        if (options & Opt::MISC::BindPose) deleteBindPoseMatrices();

        // Sanitize
        // -- Warnings
        if (sanOptions & San::WARN::SeqExts) addSequenceExtents();
        if (sanOptions & San::WARN::LightAtt) clampLightAttenuation();
        if (sanOptions & San::WARN::BindPose) addBindPoseMatrices();
    } while (oldDiffCount != diffCount);

    // Save if changes made
    if (diffCount != 0 || (options & Opt::Precision && precision >= 0)) {
        try {
            model.saveEdit();
        }
        catch(const std::exception &e) {
            emit status("Failed to optimize model: " + QString::fromStdString(e.what()), Msg::Error);
        }
    }
}

void Optimizer::doRounding(const mdlx::Animations &animations)
{
    // Model Extent
    round(model.edit.extent.boundsRadius);
    round(model.edit.extent.min);
    round(model.edit.extent.max);

    // Sequences
    for (auto &sequence : model.edit.sequences) {
        round(sequence->moveSpeed);
        round(sequence->rarity);
        round(sequence->extent.boundsRadius);
        round(sequence->extent.min);
        round(sequence->extent.max);
    }

    // Layers
    for (auto &material : model.edit.materials) {
        for (auto &layer : material->layers) {
            round(layer->alpha);
            round(layer->emissiveGain);
            round(layer->fresnelColor);
            round(layer->fresnelOpacity);
            round(layer->fresnelTeamColor);
        }
    }

    // Geosets
    for (auto &geoset : model.edit.geosets) {
        for (auto &vertex : geoset->vertices) {
            round(vertex->vertex);
            round(vertex->normal);
            for (auto &uvSet : vertex->uvSets) {
                round(uvSet);
            }
            if (vertex->haveTangent()) {
                vertex->tangent(round(vertex->tangent()));
            }
        }

        round(geoset->extent.boundsRadius);
        round(geoset->extent.min);
        round(geoset->extent.max);

        for (auto &sequenceExtent : geoset->sequenceExtents) {
            round(sequenceExtent.boundsRadius);
            round(sequenceExtent.min);
            round(sequenceExtent.max);
        }
    }

    // Geoset Animations
    for (auto &geosetAnimation : model.edit.geosetAnimations) {
        round(geosetAnimation->alpha);
        round(geosetAnimation->color);
    }

    // Pivot Points
    //round(model.edit.pivotPoints); // temp
    for (auto &genericObject : model.edit.getGenericObjects()) {
        round(genericObject->pivotPoint);
    }

    // Lights
    for (auto &light : model.edit.lights) {
        round(light->attenuation);
        round(light->color);
        round(light->intensity);
        round(light->ambientColor);
        round(light->ambientIntensity);
    }

    // Particle Emitters
    for (auto &particleEmitter : model.edit.particleEmitters) {
        round(particleEmitter->emissionRate);
        round(particleEmitter->gravity);
        round(particleEmitter->longitude);
        round(particleEmitter->latitude);
        round(particleEmitter->lifeSpan);
        round(particleEmitter->speed);
    }

    // Particle Emitters 2
    for (auto &particleEmitter2 : model.edit.particleEmitters2) {
        round(particleEmitter2->speed);
        round(particleEmitter2->variation);
        round(particleEmitter2->latitude);
        round(particleEmitter2->gravity);
        round(particleEmitter2->lifeSpan);
        round(particleEmitter2->emissionRate);
        round(particleEmitter2->width);
        round(particleEmitter2->length);
        round(particleEmitter2->tailLength);
        round(particleEmitter2->timeMiddle);
        round(particleEmitter2->segmentColors);
        round(particleEmitter2->segmentScaling);
    }

    // Particle Emitters Popcorn
    for (auto &particleEmitterPopcorn : model.edit.particleEmittersPopcorn) {
        round(particleEmitterPopcorn->lifeSpan);
        round(particleEmitterPopcorn->emissionRate);
        round(particleEmitterPopcorn->speed);
        round(particleEmitterPopcorn->color);
        round(particleEmitterPopcorn->alpha);
    }

    // Ribbon Emitters
    for (auto &ribbonEmitter : model.edit.ribbonEmitters) {
        round(ribbonEmitter->heightAbove);
        round(ribbonEmitter->heightBelow);
        round(ribbonEmitter->alpha);
        round(ribbonEmitter->color);
        round(ribbonEmitter->lifeSpan);
        round(ribbonEmitter->gravity);
    }

    // Sound Emitters
    for (auto &soundEmitter : model.edit.soundEmitters) {
        round(soundEmitter->volume);
        round(soundEmitter->pitch);
    }

    // Cameras
    for (auto &camera : model.edit.cameras) {
        round(camera->position);
        round(camera->fieldOfView);
        round(camera->farClippingPlane);
        round(camera->nearClippingPlane);
        round(camera->targetPosition);
    }

    // Collision Shapes
    for (auto &collisionShape : model.edit.collisionShapes) {
        round(collisionShape->vertices);
        round(collisionShape->boundsRadius);
    }

    // Bind Pose
    round(model.edit.bindPose);

    // Animations
    for (const auto &animation : animations) {
        switch (mdlx::animationMap[animation->tag()].second)
        {
        case mdlx::AnimType::Uint:
            break;
        case mdlx::AnimType::Float: {
            auto *anim = dynamic_cast<mdlx::AnimationEdit<float>*>(animation.get());
            for (auto &track : anim->tracks) {
                round(track.value);
                round(track.inTan);
                round(track.outTan);
            }
            break;
        }
        case mdlx::AnimType::Vec3: {
            auto *anim = dynamic_cast<mdlx::AnimationEdit<mdlx::vec3>*>(animation.get());
            for (auto &track : anim->tracks) {
                round(track.value);
                round(track.inTan);
                round(track.outTan);
            }
            break;
        }
        case mdlx::AnimType::Vec4: {
            auto *anim = dynamic_cast<mdlx::AnimationEdit<mdlx::vec4>*>(animation.get());
            for (auto &track : anim->tracks) {
                round(track.value);
                round(track.inTan);
                round(track.outTan);
            }
            break;
        }
        }
    }
}

void Optimizer::deleteZeroLengthSequences()
{
    for (size_t i=0; i<model.edit.sequences.size(); i++) {
        mdlx::uvec2 interval = model.edit.sequences[i]->interval();

        if (interval[0] >= interval[1]) {
            model.edit.sequences.erase(model.edit.sequences.begin()+i);
            i--;
        }
    }
}

void Optimizer::deleteZeroLengthGlobalSequences()
{
    for (size_t i=0; i<model.edit.globalSequences.size(); i++) {
        if (model.edit.globalSequences[i]->length == 0) {
            model.edit.deleteGlobalSequence(i);
            i--;
        }
    }
}

template<typename T>
void Optimizer::checkKeyframeUnused(mdlx::AnimationEdit<T> *animation)
{
    for (size_t i=0; i<animation->tracks.size(); i++) {
        bool found = false;

        if (animation->globalSequence) {
            found = animation->tracks[i].frame <= static_cast<int32_t>(animation->globalSequence->length);
        }
        else {
            for (auto &sequence : model.edit.sequences) {
                if (animation->tracks[i].frame >= static_cast<int32_t>(sequence->interval()[0])
                        && animation->tracks[i].frame <= static_cast<int32_t>(sequence->interval()[1]))
                {
                    found = true;
                    break;
                }
            }
        }

        if (!found) {
            animation->tracks.erase(animation->tracks.begin()+i);
            i--;
        }
    }
}

void Optimizer::deleteUnusedKeyframes(const mdlx::Animations &animations)
{
    for (auto &animation : animations) {
        switch (mdlx::animationMap[animation->tag()].second)
        {
        case mdlx::AnimType::Uint:
            checkKeyframeUnused(dynamic_cast<mdlx::AnimationEdit<uint32_t>*>(animation.get()));
            break;
        case mdlx::AnimType::Float:
            checkKeyframeUnused(dynamic_cast<mdlx::AnimationEdit<float>*>(animation.get()));
            break;
        case mdlx::AnimType::Vec3:
            checkKeyframeUnused(dynamic_cast<mdlx::AnimationEdit<mdlx::vec3>*>(animation.get()));
            break;
        case mdlx::AnimType::Vec4:
            checkKeyframeUnused(dynamic_cast<mdlx::AnimationEdit<mdlx::vec4>*>(animation.get()));
            break;
        }
    }
}

template<typename T>
void Optimizer::checkKeyframeSameish(mdlx::AnimationEdit<T> *animation)
{
    for (size_t i=0; animation->tracks.size() > 2 && i+2 < animation->tracks.size(); i++) {
        auto a = animation->tracks[i].value;
        auto b = animation->tracks[i+1].value;
        auto c = animation->tracks[i+2].value;

        bool sameish = true;
        if constexpr (std::is_same<T, uint32_t>() || std::is_same<T, float>()) {
            sameish = !(std::abs(static_cast<float>(a-b)) > 0.001 || std::abs(static_cast<float>(a-c)) > 0.001);
        }
        else {
            for (size_t j=0; j<a.size(); j++) {
                if (std::abs(a[j] - b[j]) > 0.001 || std::abs(a[j] - c[j]) > 0.001) {
                    sameish = false;
                    break;
                }
            }
        }

        if (sameish) {
            animation->tracks.erase(animation->tracks.begin() + i + 1);
            i--;
        }
    }
}

void Optimizer::deleteSameishKeyframes(const mdlx::Animations &animations)
{
    for (auto &animation : animations) {
        switch (mdlx::animationMap[animation->tag()].second)
        {
        case mdlx::AnimType::Uint:
            checkKeyframeSameish(dynamic_cast<mdlx::AnimationEdit<uint32_t>*>(animation.get()));
            break;
        case mdlx::AnimType::Float:
            checkKeyframeSameish(dynamic_cast<mdlx::AnimationEdit<float>*>(animation.get()));
            break;
        case mdlx::AnimType::Vec3:
            checkKeyframeSameish(dynamic_cast<mdlx::AnimationEdit<mdlx::vec3>*>(animation.get()));
            break;
        case mdlx::AnimType::Vec4:
            checkKeyframeSameish(dynamic_cast<mdlx::AnimationEdit<mdlx::vec4>*>(animation.get()));
            break;
        }
    }
}

void Optimizer::checkAnimationsEmpty(std::vector<std::shared_ptr<mdlx::AnimEditBase>> &animations)
{
    for (size_t i=0; i<animations.size(); i++) {
        bool empty = false;

        switch (mdlx::animationMap[animations[i]->tag()].second)
        {
        case mdlx::AnimType::Uint:
            empty = dynamic_cast<mdlx::AnimationEdit<uint32_t>*>(animations[i].get())->tracks.empty();
            break;
        case mdlx::AnimType::Float:
            empty = dynamic_cast<mdlx::AnimationEdit<float>*>(animations[i].get())->tracks.empty();
            break;
        case mdlx::AnimType::Vec3:
            empty = dynamic_cast<mdlx::AnimationEdit<mdlx::vec3>*>(animations[i].get())->tracks.empty();
            break;
        case mdlx::AnimType::Vec4:
            empty = dynamic_cast<mdlx::AnimationEdit<mdlx::vec4>*>(animations[i].get())->tracks.empty();
            break;
        }

        if (empty) {
            animations.erase(animations.begin()+i);
            i--;
        }
    }
}

void Optimizer::deleteEmptyAnimations(const mdlx::AnimatedObjects &animatedObjects)
{
    for (auto &animatedObject : animatedObjects) {
        checkAnimationsEmpty(animatedObject->animations);
    }
}

void Optimizer::linearizeAnimations(const mdlx::Animations &animations)
{
    for (auto &animation : animations) {
        if (animation->interpolationType() > mdlx::AnimBase::Linear) {
            animation->interpolationType(mdlx::AnimBase::Linear);
        }
    }
}

void Optimizer::deleteUnusedMaterials()
{
    for (size_t i=0; i<model.edit.materials.size(); i++) {
        bool found = false;

        // Search for a ribbon emitter that uses current material
        for (const auto &ribbonEmitter : model.edit.ribbonEmitters) {
            if (ribbonEmitter->material == model.edit.materials[i]) {
                found = true;
                break;
            }
        }

        if (!found) {
            // Search for a geoset that uses current material
            for (const auto &geoset : model.edit.geosets) {
                if (geoset->material == model.edit.materials[i]) {
                    found = true;
                    break;
                }
            }
        }

        // Delete the material if no geoset or ribbon emitter found
        if (!found && model.edit.deleteMaterial(i, true)) {
            diffCount++;
            i--;
        }
    }
}

void Optimizer::mergeIdenticalMaterials()
{
    for (size_t i=0; i<model.edit.materials.size(); i++) {
        for (size_t j=i+1; j<model.edit.materials.size(); j++) {
            // Materials match
            if (*model.edit.materials[i] == *model.edit.materials[j]) {
                // Change geosets materialId
                for (auto &geoset : model.edit.geosets) {
                    if (geoset->material == model.edit.materials[j]) {
                        geoset->material = model.edit.materials[i];
                    }
                }

                // Change ribbon emitters materialId
                for (auto &ribbonEmitter : model.edit.ribbonEmitters) {
                    if (ribbonEmitter->material == model.edit.materials[j]) {
                        ribbonEmitter->material = model.edit.materials[i];
                    }
                }

                // Delete matching material
                if (model.edit.deleteMaterial(j, true)) {
                    j--;
                }

                diffCount++;
            }
        }
    }
}

void Optimizer::deleteUnusedTextures()
{
    for (size_t i=0; i<model.edit.textures.size(); i++) {
        bool found = false;

        // Search Particle Emitters 2
        for (const auto &particleEmitter2 : model.edit.particleEmitters2) {
            if (particleEmitter2->texture == model.edit.textures[i]) {
                found = true;
                break;
            }
        }

        if (!found) {
            // Search Layers
            for (const auto &material : model.edit.materials) for (const auto &layer : material->layers) {
                if (layer->texture == model.edit.textures[i]) {
                    found = true;
                    break;
                }
            }
        }

        // Not used -> Delete
        if (!found && model.edit.deleteTexture(i, true)) {
            diffCount++;
            i--;
        }
    }
}

void Optimizer::mergeIdenticalTextures()
{
    for (size_t i=0; i<model.edit.textures.size(); i++) {
        for (size_t j=i+1; j<model.edit.textures.size(); j++) {
            // Textures Match
            if (*model.edit.textures[i] == *model.edit.textures[j]) {
                // Change Layer Textures
                for (auto &material : model.edit.materials) for (auto &layer : material->layers) {
                    if (layer->texture == model.edit.textures[j]) {
                        layer->texture = model.edit.textures[i];
                    }
                }

                // Change Particle Emitter 2 Textures
                for (auto &particleEmitter2 : model.edit.particleEmitters2) {
                    if (particleEmitter2->texture == model.edit.textures[j]) {
                        particleEmitter2->texture = model.edit.textures[i];
                    }
                }

                // Delete matching Texture
                if (model.edit.deleteTexture(j, true)) {
                    j--;
                }

                diffCount++;
            }
        }
    }
}

void Optimizer::deleteUnusedTextureAnimations()
{
    for (size_t i=0; i<model.edit.textureAnimations.size(); i++) {
        bool found = false;

        // Search layers
        for (const auto &material : model.edit.materials) for (const auto &layer : material->layers) {
            if (layer->textureAnimation == model.edit.textureAnimations[i]) {
                found = true;
                break;
            }
        }

        // Not used -> Delete
        if (!found && model.edit.deleteTextureAnimation(i, true)) {
            diffCount++;
            i--;
        }
    }
}

void Optimizer::mergeIdenticalTextureAnimations()
{
    for (size_t i=0; i<model.edit.textureAnimations.size(); i++) {
        for (size_t j=i+1; j<model.edit.textureAnimations.size(); j++) {
            // Texture Animations match
            if (*model.edit.textureAnimations[i] == *model.edit.textureAnimations[j]) {
                // Change Layers Texture Animations
                for (auto &material : model.edit.materials) for (auto &layer : material->layers) {
                    if (layer->textureAnimation == model.edit.textureAnimations[j]) {
                        layer->textureAnimation = model.edit.textureAnimations[i];
                    }
                }

                // Delete matching Texture Animation
                if (model.edit.deleteTextureAnimation(j, true)) {
                    j--;
                }

                diffCount++;
            }
        }
    }
}

void Optimizer::deleteFakeTriangles()
{
    for (auto &geoset : model.edit.geosets) {
        // Find triangles with at least one duplicate vertex, but leave at least one triangle
        for (uint32_t i=0; i<geoset->triangles.size() && geoset->triangles.size() > 1; i++) {
            if ((geoset->triangles[i][0] == geoset->triangles[i][1]
                 || geoset->triangles[i][0] == geoset->triangles[i][2]
                 || geoset->triangles[i][1] == geoset->triangles[i][2])
                    && geoset->deleteTriangle(i))
            {
                diffCount++;
                i--;
            }
        }
    }
}

void Optimizer::deleteFreeVertices()
{
    for (size_t i=0; i<model.edit.geosets.size(); i++) {
        // For each vertex, find a triangle that uses it
        for (size_t j=0; j<model.edit.geosets[i]->vertices.size(); j++) {
            bool found = false;

            for (const auto &triangle : model.edit.geosets[i]->triangles) {
                if (triangle[0] == model.edit.geosets[i]->vertices[j]
                        || triangle[1] == model.edit.geosets[i]->vertices[j]
                        || triangle[2] == model.edit.geosets[i]->vertices[j])
                {
                    found = true;
                    break;
                }
            }

            // None found -> delete
            if (!found && model.edit.geosets[i]->deleteVertex(model.edit.geosets[i]->vertices[j]->index, true)) {
                diffCount++;
                j--;
            }
        }

        if (model.edit.geosets[i]->vertices.empty() && model.edit.deleteGeoset(model.edit.geosets[i]->index)) {
            diffCount++;
            i--;
        }
    }
}

void Optimizer::mergeIdenticalVertices()
{
    for (auto &geoset : model.edit.geosets) {
        // Iterate vertices
        for (size_t i=0; i<geoset->vertices.size(); i++) {
            mdlx::vec3 normalsSum{};
            uint32_t normalsCount = 0;

            if (!(options & Opt::GEOS::PreserveNrms)) {
                normalsSum = geoset->vertices[i]->normal;
                normalsCount++;
            }

            // Iterate subsequent vertices to find matches
            for (size_t j=i+1; j<geoset->vertices.size(); j++) {
                // If vertex matches
                if (*geoset->vertices[i] == *geoset->vertices[j]
                        && (!(options & Opt::GEOS::PreserveNrms) || mdlx::compf(geoset->vertices[i]->normal, geoset->vertices[j]->normal)))
                {
                    // Replace triangle references
                    for (auto &triangle : geoset->triangles) for (auto &vtx : triangle) {
                        if (vtx == geoset->vertices[j]) {
                            vtx = geoset->vertices[i];
                        }
                    }

                    if (!(options & Opt::GEOS::PreserveNrms)) {
                        normalsSum[0] += geoset->vertices[j]->normal[0];
                        normalsSum[1] += geoset->vertices[j]->normal[1];
                        normalsSum[2] += geoset->vertices[j]->normal[2];
                        normalsCount++;
                    }

                    // Delete merged vertex
                    if (geoset->deleteVertex(geoset->vertices[j]->index, true)) {
                        j--;
                    }

                    diffCount++;
                }
            }

            if (!(options & Opt::GEOS::PreserveNrms) && normalsCount > 1) {
                geoset->vertices[i]->normal[0] = normalsSum[0] / normalsCount;
                geoset->vertices[i]->normal[1] = normalsSum[1] / normalsCount;
                geoset->vertices[i]->normal[2] = normalsSum[2] / normalsCount;

                if (options & Opt::Precision && precision >= 0) round(geoset->vertices[i]->normal);
            }
        }
    }
}

void Optimizer::deleteUnusedMatrixGroups()
{
    for (auto &geoset : model.edit.geosets) {
        // Loop through all matrixGroups
        for (size_t i=0; i<geoset->matrixGroups.size(); i++) {
            bool match = false;
            for (const auto &vertex : geoset->vertices) {
                if (vertex->vertexGroup == geoset->matrixGroups[i]) {
                    match = true;
                    break;
                }
            }

            if (!match && geoset->deleteMatrixGroup(geoset->matrixGroups[i]->index, true)) {
                diffCount++;
                i--;
            }
        }
    }
}

void Optimizer::deleteSequenceExtents()
{
    for (auto &geoset : model.edit.geosets) {
        size_t origSize = geoset->sequenceExtents.size();
        size_t targetSize = geoset->sequenceExtents.size();

        if (origSize > targetSize) {
            diffCount += origSize - targetSize;
            geoset->sequenceExtents.resize(targetSize, geoset->sequenceExtents.back());
        }
    }
}

void Optimizer::mergeSimilarGeosets()
{
    for (size_t i=0; i<model.edit.geosets.size(); i++) {
        // Ignore geosets referred to by bones
        bool boneFound = false;
        for (auto &bone : model.edit.bones) {
            if (bone->geoset == model.edit.geosets[i]) {
                boneFound = true;
                break;
            }
        }

        if (!boneFound) {
            for (size_t j=i+1; j<model.edit.geosets.size(); j++) {
                // Match found
                if (*model.edit.geosets[i] == *model.edit.geosets[j]) {
                    // Ignore geosets referred to by bones
                    boneFound = false;
                    for (auto &bone : model.edit.bones) {
                        if (bone->geoset == model.edit.geosets[j]) {
                            boneFound = true;
                            break;
                        }
                    }

                    if (!boneFound) {
                        auto origGeosetAnim = model.edit.getGeosetAnimation(i);
                        auto mergedGeosetAnim = model.edit.getGeosetAnimation(j);

                        // Geoset Animations match
                        if ((bool)origGeosetAnim == (bool)mergedGeosetAnim && (!origGeosetAnim || *origGeosetAnim == *mergedGeosetAnim)) {
                            size_t origMtxSize = model.edit.geosets[i]->matrixGroups.size();
                            uint32_t index = origMtxSize;

                            // Merge Matrix Groups
                            model.edit.geosets[i]->matrixGroups.reserve(origMtxSize + model.edit.geosets[j]->matrixGroups.size());
                            for (auto &matrixGroup : model.edit.geosets[j]->matrixGroups) {
                                auto newGroup = model.edit.geosets[i]->matrixGroups.emplace_back(matrixGroup);
                                newGroup->index = index;
                                index++;
                            }

                            size_t origVtxSize = model.edit.geosets[i]->vertices.size();
                            size_t newVtxSize = origVtxSize + model.edit.geosets[j]->vertices.size();
                            index = origVtxSize;

                            // Merge vertices
                            model.edit.geosets[i]->vertices.reserve(newVtxSize);
                            for (auto &vertex : model.edit.geosets[j]->vertices) {
                                auto newVertex = model.edit.geosets[i]->vertices.emplace_back(vertex);
                                newVertex->index = index;
                                index++;
                            }

                            // Merge triangles
                            model.edit.geosets[i]->triangles.insert(model.edit.geosets[i]->triangles.end(),
                                                                    model.edit.geosets[j]->triangles.begin(),
                                                                    model.edit.geosets[j]->triangles.end());

                            // Merge identical Matrix Groups
                            for (size_t k=0; k<origMtxSize; k++) {
                                for (size_t l=origMtxSize; l<model.edit.geosets[i]->matrixGroups.size(); l++) {
                                    if (*model.edit.geosets[i]->matrixGroups[k] == *model.edit.geosets[i]->matrixGroups[l]) {
                                        for (size_t m=origVtxSize; m<newVtxSize; m++) {
                                            if (model.edit.geosets[i]->vertices[m]->vertexGroup == model.edit.geosets[i]->matrixGroups[l]) {
                                                model.edit.geosets[i]->vertices[m]->vertexGroup = model.edit.geosets[i]->matrixGroups[k];
                                            }
                                        }

                                        model.edit.geosets[i]->deleteMatrixGroup(l, true);
                                        l--;
                                    }
                                }
                            }

                            // Take largest extent values
                            model.edit.geosets[i]->extent.boundsRadius = std::max(model.edit.geosets[i]->extent.boundsRadius,
                                                                                  model.edit.geosets[j]->extent.boundsRadius);
                            for (size_t k=0; k<3; k++) {
                                model.edit.geosets[i]->extent.min[k] = std::max(model.edit.geosets[i]->extent.min[k],
                                                                                model.edit.geosets[j]->extent.min[k]);
                                model.edit.geosets[i]->extent.max[k] = std::max(model.edit.geosets[i]->extent.max[k],
                                                                                model.edit.geosets[j]->extent.max[k]);
                            }

                            for (size_t s=0; s<model.edit.geosets[i]->sequenceExtents.size()
                                                && s<model.edit.geosets[j]->sequenceExtents.size(); s++)
                            {
                                model.edit.geosets[i]->sequenceExtents[s].boundsRadius
                                        = std::max(model.edit.geosets[i]->sequenceExtents[s].boundsRadius,
                                                   model.edit.geosets[j]->sequenceExtents[s].boundsRadius);
                                for (size_t k=0; k<3; k++) {
                                    model.edit.geosets[i]->sequenceExtents[s].min[k]
                                            = std::max(model.edit.geosets[i]->sequenceExtents[s].min[k],
                                                       model.edit.geosets[j]->sequenceExtents[s].min[k]);
                                    model.edit.geosets[i]->sequenceExtents[s].max[k]
                                            = std::max(model.edit.geosets[i]->sequenceExtents[s].max[k],
                                                       model.edit.geosets[j]->sequenceExtents[s].max[k]);
                                }
                            }

                            // Delete geoset animations if not refered to by a bone
                            if (mergedGeosetAnim) {
                                bool found = false;
                                for (auto &bone : model.edit.bones) {
                                    if (bone->geosetAnimation == mergedGeosetAnim) {
                                        found = true;
                                    }
                                }

                                if (!found) {
                                    model.edit.deleteGeosetAnimation(mergedGeosetAnim->index);
                                }
                            }

                            if (model.edit.deleteGeoset(j)) {
                                j--;
                            }

                            diffCount++;
                        } // Geoset Animation match
                    } // Subloop !boneFound
                } // Geoset match
            } // Subloop
        } // !boneFound
    } // Main loop
}

void Optimizer::deleteUnusedGeosetAnimations()
{
    for (size_t i=0; i<model.edit.geosetAnimations.size(); i++) {
        // No Geoset set
        if (!model.edit.geosetAnimations[i]->geoset) {
            // Find a bone reference
            bool found = false;
            for (const auto &bone : model.edit.bones) {
                if (bone->geosetAnimation && bone->geosetAnimation == model.edit.geosetAnimations[i]) {
                    found = true;
                    break;
                }
            }

            // Delete
            if (!found && model.edit.deleteGeosetAnimation(i, true)) {
                diffCount++;
                i--;
            }
        }
    }
}

void Optimizer::deleteDuplicateGeosetAnimations()
{
    for (size_t i=0; i<model.edit.geosetAnimations.size(); i++) {
        for (size_t j=i+1; j<model.edit.geosetAnimations.size(); j++) {
            // Match found
            if (model.edit.geosetAnimations[i]->geoset == model.edit.geosetAnimations[j]->geoset) {
                // Find bones
                bool found = false;
                for (const auto &bone : model.edit.bones) {
                    if (bone->geosetAnimation == model.edit.geosetAnimations[j]) {
                        found = true;
                        break;
                    }
                }

                // Used by bone(s) -> unset geoset
                if (found) {
                    model.edit.geosetAnimations[j]->geoset = nullptr;
                }
                //Not used by bone(s) -> delete
                else {
                    model.edit.deleteGeosetAnimation(j, true);
                }

                diffCount++;
            }
        }
    }
}

void Optimizer::deleteUnusedBonesHelpers()
{
    for (size_t i=0; i<model.edit.bones.size(); i++) {
        if (!model.edit.isGenericObjectInUse(model.edit.bones[i]->objectId) && model.edit.deleteBone(i, true)) {
            diffCount++;
            i--;
        }
    }

    for (size_t i=0; i<model.edit.helpers.size(); i++) {
        if (!model.edit.isGenericObjectInUse(model.edit.helpers[i]->objectId) && model.edit.deleteHelper(i, true)) {
            diffCount++;
            i--;
        }
    }
}

void Optimizer::deleteUnusedEventObjects()
{
    for (size_t i=0; i<model.edit.eventObjects.size(); i++) {
        if (model.edit.eventObjects[i]->tracks.empty() && !model.edit.isGenericObjectInUse(model.edit.eventObjects[i]->objectId)
                && model.edit.deleteEventObject(i))
        {
            diffCount++;
            i--;
        }
    }
}

void Optimizer::deleteBindPoseMatrices()
{
    size_t targetSize = model.edit.getGenericObjects().size();

    if (model.edit.bindPose.size() > targetSize) {
        diffCount++;
        model.edit.bindPose.resize(targetSize);
    }
}

void Optimizer::addSequenceExtents()
{
    for (auto &geoset : model.edit.geosets) {
        size_t origSize = geoset->sequenceExtents.size();
        size_t targetSize = model.edit.sequences.size();

        if (origSize < targetSize) {
            diffCount += targetSize - origSize;

            if (geoset->sequenceExtents.empty()) {
                geoset->sequenceExtents.resize(targetSize, geoset->extent);
            }
            else geoset->sequenceExtents.resize(targetSize, geoset->sequenceExtents.back());
        }
    }
}

void Optimizer::clampLightAttenuation()
{
    for (auto &light : model.edit.lights) {
        if (light->attenuation[0] < 80) {
            light->attenuation[0] = 80;
            diffCount++;
        }
        if (light->attenuation[1] > 200) {
            light->attenuation[1] = 200;
            diffCount++;
        }
    }
}

void Optimizer::addBindPoseMatrices()
{
    size_t targetSize = model.edit.getGenericObjects().size();

    if (model.edit.bindPose.size() < targetSize) {
        diffCount++;

        if (model.edit.bindPose.empty()) {
            model.edit.bindPose.resize(targetSize);
        }
        else model.edit.bindPose.resize(targetSize, model.edit.bindPose.back());
    }
}
