#ifndef COMMON_H
#define COMMON_H

#include <QString>

namespace Msg
{
constexpr uint8_t None      = 0;
constexpr uint8_t Notice    = 1;
constexpr uint8_t Success   = 2;
constexpr uint8_t Error     = 3;
}

extern QString cleanPath(const QString &path);

#endif // COMMON_H
