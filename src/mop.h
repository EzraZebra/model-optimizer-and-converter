#ifndef MOP_H
#define MOP_H

#include <QThread>

/** @brief The main class for the application. Launches CMD or Window, and mediates between them and Converter. */
class Mop : public QObject
{
    Q_OBJECT

    QThread thread;

public:
    Mop();
    ~Mop();

    template<class APP>
    void init(APP &app);

    /** @brief Move a Converter to a QThread, connect signals, and start conversion. */
    template<class APP>
    void process(APP &app, const QStringList &paths, const QString &target,
                 uint8_t owOptions, uint8_t procOptions, uint8_t mdlxOptions,
                 uint32_t optOptions, uint32_t sanOptions, int precision);

public slots:
    /** @brief Quit the thread and wait(). */
    void quit();

signals:
    void startProcess();
};

#include "mop.tcc"

#endif // MOP_H
