QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    mop.cpp \
    optimizer.cpp \
    options.cpp \
    processor.cpp \
    statustext.cpp \
    util.cpp

HEADERS += \
    mainwindow.h \
    mainwindow.tcc \
    mop.h \
    mop.tcc \
    optimizer.h \
    optimizer.tcc \
    options.h \
    processor.h \
    statustext.h \
    util.h

installs.path = $$OUT_PWD
installs.files += $$PWD/style.css
INSTALLS += installs

RC_ICONS += \
    ../res/mop.ico

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../MdlxParser/build-MdlxParser/release/ -lMdlxParser
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../MdlxParser/build-MdlxParser/debug/ -lMdlxParser

INCLUDEPATH += $$PWD/../../MdlxParser/src
DEPENDPATH += $$PWD/../../MdlxParser/src

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../MdlxParser/build-MdlxParser/release/libMdlxParser.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../MdlxParser/build-MdlxParser/debug/libMdlxParser.a
