#include "processor.h"
#include "optimizer.h"

#include <model.h>

#include <QDir>
#include <QTimer>

Processor::Processor(const QStringList &pathList, const QString &targetPath,
                     uint8_t overWrite, uint8_t procOptions, uint8_t mdlxOptions,
                     uint32_t optOptions, uint32_t sanOptions, int precision)
    : pathList(pathList), targetPath(targetPath),
      overWrite(overWrite), procOptions(procOptions), mdlxOptions(mdlxOptions),
      optOptions(optOptions), sanOptions(sanOptions),
      precision(precision) {}

void Processor::process()
{
    bool interrupted = false; // Set to true if confirm signal is emitted and loop is broken

    while (!pathList.isEmpty()) {
        QFileInfo fileInfo(pathList.first());
        QString path = cleanPath(pathList.first());

        // Check if path exists
        if (!fileInfo.exists()) emit status("Not found:<br />" + path, Msg::Error);

        // File -> try processing
        else if (fileInfo.isFile()) {
            QString suffix = fileInfo.suffix().toLower();

            if (suffix != "mdx" && suffix != "mdl") {
                emit status("Invalid extension:<br />" + path, Msg::Error);
            }
            else if ((procOptions & Proc::Mdx && suffix == "mdx") || (procOptions & Proc::Mdl && suffix == "mdl")) {
                QString targetPath = cleanPath(this->targetPath.isEmpty() ? fileInfo.absolutePath() : this->targetPath);
                QString targetFile = fileInfo.completeBaseName();
                QString interfix = "_";
                QString target;

                if (!targetPath.endsWith(QDir::separator())) {
                    targetPath.append(QDir::separator());
                }

                if (procOptions & Proc::Convert && procOptions & Proc::SuffixConv) {
                    targetFile.append("_conv");
                    interfix = "";
                }
                if (procOptions & Proc::Optimize && procOptions & Proc::SuffixOpt) {
                    targetFile.append("_opt");
                    interfix = "";
                }

                if (procOptions & Proc::Convert) {
                    suffix = suffix == "mdx" ? ".mdl" : ".mdx";
                }
                else suffix = suffix == "mdx" ? ".mdx" : ".mdl";

                target = targetPath + targetFile + suffix;
                bool targetOk = !QFileInfo::exists(target);

                // Check if confirmation is needed
                if (!targetOk) {
                    if (overWrite == OW::Confirm) {
                        emit confirm(target, pathList.size() > 1);
                        interrupted = true;
                        break;
                    }
                    else if (overWrite == OW::Incr || overWrite == OW::IncrAll) {
                        for (int i=2; QFileInfo::exists(target); i++) {
                            target = targetPath + targetFile + interfix + QString::number(i) + suffix;
                        }

                        targetOk = true;
                    }
                    else if (overWrite == OW::Yes || overWrite == OW::YesAll) {
                        targetOk = true;
                    }

                    if (overWrite == OW::Yes || overWrite == OW::Incr || overWrite == OW::No) {
                        overWrite = OW::Confirm;
                    }
                }

                if (!targetOk) {
                    emit status("File skipped:<br />" + path);
                    skipCount++;
                }
                else {
                    mdlx::Model model;

                    emit status("Loading model:<br />" + path + " ...");

                    // Try to load the model
                    try {
                        model.load(path.toStdString());
                    }
                    catch (const std::exception &e) {
                        emit status("Exception: " + QString::fromStdString(e.what()), Msg::Error);
                    }

                    if (!success(model)) {
                        emit status("<br />Loading failed.", Msg::Error);
                        failCount++;
                    }
                    else {
                        // Optimize
                        if (procOptions & Proc::Optimize) {
                            emit status("Optimizing...", Msg::Notice);

                            try {
                                Optimizer optimizer(model, optOptions, sanOptions, precision);
                                connect(&optimizer, &Optimizer::status, this, &Processor::status);
                                optimizer.optimize();
                            }
                            catch (const std::exception &e) {
                                emit status("Exception: " + QString::fromStdString(e.what()), Msg::Error);
                            }
                        }

                        if (!success(model)) {
                            emit status("<br />Optimization failed.", Msg::Error);
                            failCount++;
                        }
                        else {
                            if (procOptions & Proc::Convert) {
                                emit status("Converting...", Msg::Notice);
                            }
                            else emit status("Saving...", Msg::Notice);

                            QFileInfo targetFO(target);

                            // If target dir does not exist, create it
                            const QString targetDir = targetFO.absolutePath();
                            if (!QDir().exists(targetDir)) {
                                QDir().mkpath(targetDir);
                            }

                            // Try to save the model
                            try {
                                model.save(target.toStdString(), mdlxOptions);
                            }
                            catch (const std::exception &e) {
                                emit status("Exception: " + QString::fromStdString(e.what()), Msg::Error);
                            }

                            if (!success(model)) {
                                emit status("<br />Saving failed.", Msg::Error);
                                failCount++;
                            }
                            else {
                                emit status("Saved as:<br />" + target, Msg::Success);
                                successCount++;

                                if (procOptions & Proc::Optimize && !(procOptions & Proc::Convert)) {
                                    size_t origSize = fileInfo.size();
                                    size_t optSize = targetFO.size();
                                    QString savedStr = sizeDiff(origSize, optSize);

                                    if (!savedStr.isEmpty()) {
                                        totalSize += origSize;
                                        totalSizeOpt += optSize;
                                        emit status("Filesize reduced by " + savedStr + ".", Msg::Success);
                                    }
                                }
                            }
                        }
                    }
                } // targetOk
            } // valid extension
        }

        // Directory -> search for files and folders and add to pathList
        else if (fileInfo.isDir()) {
            QDir::Filters filters = QDir::NoDotAndDotDot|QDir::Files;
            if (procOptions & Proc::Recursive) filters |= QDir::AllDirs;

            QStringList entryList = QDir(path).entryList({ "*.mdx", "*.mdl" }, filters);

            if (!path.endsWith(QDir::separator())) {
                path.append(QDir::separator());
            }

            int i = 0;
            for (const auto &entry : entryList) {
                i++;
                pathList.insert(i, path + entry);
            }
        }
        else emit status("Invalid path:<br />" + path, Msg::Error);

        pathList.pop_front();
    }

    if (!interrupted) {
        QString br = "<br />";
        bool skip = successCount != 0 && skipCount != 0;
        bool fail = !skip && failCount != 0;
        bool success = !fail && successCount != 0;
        bool saved = success;

        if (successCount != 0) {
            QString savedStr = successCount == 1 ? "" : sizeDiff(totalSize, totalSizeOpt);
            success = savedStr.isEmpty() && success;

            emit status(br + QString::number(successCount) + " " + (successCount == 1 ? "file" : "files") + " successfully processed.",
                        Msg::Success, saved);
            br = "";

            if (!savedStr.isEmpty()) {
                emit status("Total filesize reduction: " + savedStr + ".", Msg::Success);
            }
        }
        if (failCount != 0) {
            emit status(br + QString::number(failCount) + " " + (failCount == 1 ? "file" : "files") + " failed to process.",
                        Msg::Error, fail);
            br = "";
        }
        if (skipCount != 0) {
            emit status(br + QString::number(skipCount) + " " + (skipCount == 1 ? "file" : "files") + " skipped.",
                        Msg::Notice, skip);
            br = "";
        }
        if (successCount == 0) {
            emit status(br + "No files processed.", Msg::Notice, true);
        }
    }
}

void Processor::confirmed(uint8_t btn)
{
    overWrite = btn;
    process();
}

bool Processor::success(mdlx::Model &model)
{
    for (const auto &msg : model.errors.messages()) {
        emit status("Error: " + QString::fromStdString(msg), Msg::Error);
    }

    bool success = !model.errors.failed();
    model.errors.clear();
    return success;
}

QString Processor::sizeDiff(size_t orig, size_t opt) const
{
    float origSize = static_cast<float>(orig);
    float savedSize = origSize - static_cast<float>(opt);
    float savedPct = savedSize / origSize * 100;

    if (savedSize != 0) {
        int div = 0;
        while (savedSize >= 1024 && div < 3) {
            savedSize /= 1024;
            div++;
        }

        QString savedStr;
        switch(div) {
        case 0: savedStr = QString::number(savedSize) + "B";
            break;
        case 1: savedStr = QString::number(savedSize, 'f', 2) + "KiB";
            break;
        case 2: savedStr = QString::number(savedSize, 'f', 2) + "MiB";
            break;
        }

        return savedStr + " (" + QString::number(savedPct, 'f', 2) + "%)";
    }

    return QString();
}
