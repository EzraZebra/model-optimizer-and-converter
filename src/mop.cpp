#include "mop.h"

Mop::Mop()
{
    qRegisterMetaType<uint8_t>("uint8_t");
}

Mop::~Mop()
{
    quit();
}

void Mop::quit()
{
    thread.quit();
    thread.wait();
}
