#ifndef STATUSTEXT_H
#define STATUSTEXT_H

#include "util.h"

#include <QTextEdit>

/** @brief A read-only QTextEdit for displaying status messages. */
struct StatusText : public QTextEdit
{
    StatusText();
    ~StatusText() = default;

    /** @brief Append a message. */
    void show(QString msg, uint8_t type = Msg::Notice);
    /** @brief Append a horizontal ruler. */
    void separator();

private:
    uint8_t previousType = Msg::None;
};

#endif // STATUSTEXT_H
