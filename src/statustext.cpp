#include "statustext.h"

StatusText::StatusText()
{
    setReadOnly(true);
}

void StatusText::show(QString msg, uint8_t type)
{
    if (previousType != Msg::None && previousType != type) msg.prepend("<br />");

    switch(type) {
    case Msg::Success: append("<span style='color:green; font-weight:bold;'>" + msg + "</span>");
        break;
    case Msg::Error: append("<span style='color:red; font-weight:bold;'>" + msg + "</span>");
        break;
    case Msg::Notice: append("<span font-weight:bold;'>" + msg + "</span>");
        break;
    }

    previousType = type;
}

void StatusText::separator()
{
    append("<hr><br />");
    previousType = Msg::None;
}
