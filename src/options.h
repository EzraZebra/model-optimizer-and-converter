#ifndef OPTIONS_H
#define OPTIONS_H

#include <model_options.h>

#include <map>

#include <QStringList>

namespace OW
{
constexpr uint8_t Confirm    = 0;
constexpr uint8_t Yes        = 1;
constexpr uint8_t YesAll     = 2;
constexpr uint8_t Incr       = 3;
constexpr uint8_t IncrAll    = 4;
constexpr uint8_t No         = 5;
constexpr uint8_t NoAll      = 6;
}

struct Proc
{
    static constexpr uint8_t Recursive  = 1 << 0;
    static constexpr uint8_t Mdx        = 1 << 1;
    static constexpr uint8_t Mdl        = 1 << 2;
    static constexpr uint8_t Optimize   = 1 << 3;
    static constexpr uint8_t Sanitize   = 1 << 4;
    static constexpr uint8_t Convert    = 1 << 5;
    static constexpr uint8_t SuffixOpt  = 1 << 6;
    static constexpr uint8_t SuffixConv = 1 << 7;

    static std::map<uint8_t, std::pair<QStringList, bool>> map;
    static std::pair<QStringList, bool> suffixOpt;
    static std::pair<QStringList, bool> suffixConv;
};

struct Mdlx
{
    static constexpr uint8_t Version = 1 << 4;

    static std::pair<QStringList, bool> version;
    static std::map<uint8_t, std::pair<QStringList, bool>> map;
};

struct Version
{
    static const QStringList str;
    static std::map<QString, uint8_t> string2Flag;
};

struct Opt
{
    static constexpr uint32_t Precision = 1 << 0;

    static std::pair<QStringList, bool> precision;

    struct SEQS
    {
        static constexpr uint32_t Linearize         = 1 << 1;
        static constexpr uint32_t DeleteEmpty       = 1 << 2;
        static constexpr uint32_t DeleteUnused      = 1 << 3;
        static constexpr uint32_t DeleteSameish     = 1 << 4;
        static constexpr uint32_t ZeroLength        = 1 << 5;
        static constexpr uint32_t ZeroLengthGlob    = 1 << 6;

        static std::map<uint32_t, std::pair<QStringList, bool>> map;
    };

    struct MTLS
    {
        static constexpr uint32_t Delete    = 1 << 7;
        static constexpr uint32_t Merge     = 1 << 8;
    };

    struct TEXS
    {
        static constexpr uint32_t Delete    = 1 << 9;
        static constexpr uint32_t Merge     = 1 << 10;
    };

    struct TXAN
    {
        static constexpr uint32_t Delete    = 1 << 11;
        static constexpr uint32_t Merge     = 1 << 12;
    };

    static std::map<uint32_t, std::pair<QStringList, bool>> mtMap;

    struct GEOS
    {
        static constexpr uint32_t VtxDelete         = 1 << 13;
        static constexpr uint32_t VtxMerge          = 1 << 14;
        static constexpr uint32_t PreserveNrms      = 1 << 15;
        static constexpr uint32_t Tris              = 1 << 16;
        static constexpr uint32_t MtxGrpDelete      = 1 << 17;
        static constexpr uint32_t SeqExt            = 1 << 18;
        static constexpr uint32_t Merge             = 1 << 19;
        static constexpr uint32_t GeoAnimDelete     = 1 << 20;
        static constexpr uint32_t GeoAnimDuplDelete = 1 << 21;

        static std::map<uint32_t, std::pair<QStringList, bool>> map;
    };

    struct NODS
    {
        static constexpr uint32_t BoneHelperDelete  = 1 << 22;
        static constexpr uint32_t EventObjDelete    = 1 << 23;

        static std::map<uint32_t, std::pair<QStringList, bool>> map;
    };

    struct MISC
    {
        static constexpr uint32_t UnknownChunksDelete   = 1 << 24;
        static constexpr uint32_t BindPose              = 1 << 25;

        static std::map<uint32_t, std::pair<QStringList, bool>> map;
    };
};

struct San
{
    struct WARN
    {
        static constexpr uint32_t SeqExts   = 1 << 1;
        static constexpr uint32_t LightAtt  = 1 << 2;
        static constexpr uint32_t BindPose  = 1 << 3;

        static std::map<uint32_t, std::pair<QStringList, bool>> map;
    };
};

#endif // OPTIONS_H
