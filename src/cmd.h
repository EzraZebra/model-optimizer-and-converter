#ifndef CMD_H
#define CMD_H

#include "mop.h"

#include <QCommandLineParser>

using optionMap = std::map<QStringList, std::pair<int, bool>>;

/** @brief Parse and process command line */
class Cmd : public QObject
{
    Q_OBJECT

    static std::unordered_map<std::string, const int> confirmString2Int;

    Mop mop;
    QCommandLineParser parser;
    int previousType;

    void addParserOption(const QStringList &list);
    template<typename T>
    void addParserOptions(const std::map<T, std::pair<QStringList, bool>> &map);

    template<typename T>
    void addOption(T &options, T id, const std::pair<QStringList, bool> &pair) const;
    template<typename T>
    void addOptions(T &options, const std::map<T, std::pair<QStringList, bool>> &map) const;

public:
    Cmd(int mode);

public slots:
    /** @brief Ask the user for overwrite confirmation and send the answer to the converter */
    void confirm(const QString &path, bool multiple);
    /** @brief Output a message and `quit()` if finished */
    void showStatus(const QString &msg, uint8_t type, bool finished);

signals:
    void confirmed(uint8_t btn);
    void processFinished();
};

#include "cmd.tcc"

#endif // CMD_H
