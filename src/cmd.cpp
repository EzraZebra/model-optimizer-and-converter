#include "util.h"
#include "options.h"
#include "cmd.h"

#include <iostream>

std::unordered_map<std::string, const int> Cmd::confirmString2Int = {
    { "yes", OW::Yes },
    { "y", OW::Yes },
    { "yesall", OW::YesAll },
    { "ya", OW::YesAll },
    { "increment", OW::Incr },
    { "i", OW::Incr },
    { "incrementall", OW::IncrAll },
    { "ia", OW::IncrAll },
    { "no", OW::No },
    { "n", OW::No },
    { "noall", OW::NoAll },
    { "na", OW::NoAll },
};

Cmd::Cmd(int mode)
{
    if (mode & Proc::Convert) {
        QCoreApplication::instance()->setApplicationName("MDLX Converter");
        parser.setApplicationDescription("Convert between MDL and MDX files.");
    }
    else if (mode & Proc::Optimize) {
        QCoreApplication::instance()->setApplicationName("MDLX Optimizer");
        parser.setApplicationDescription("Optimize MDL and MDX files.");
    }

    parser.addHelpOption();
    parser.addPositionalArgument("source", "Files to be converted, or folders to search for files.");
    parser.addOptions({
                          { "destination", "Destination folder or file.", "destination" },
                          { "conflict", "File conflict handling. [ASK|OVERWRITE|SKIP|INCREMENT]\nDefault: ASK", "conflict" },
                      });

    addParserOptions(Proc::map);
    addParserOption(Mdlx::version.first);
    addParserOptions(Mdlx::map);

    if (mode & Proc::Convert) {
        addParserOption(Proc::suffixConv.first);
    }

    if (mode & Proc::Optimize) {
        addParserOption(Proc::suffixOpt.first);
        addParserOption(Opt::precision.first);
        addParserOptions(Opt::SEQS::map);
        addParserOptions(Opt::mtMap);
        addParserOptions(Opt::GEOS::map);
        addParserOptions(Opt::NODS::map);
        addParserOptions(Opt::MISC::map);
        addParserOptions(San::WARN::map);
    }

    parser.process(*QCoreApplication::instance());

    QStringList pathList = parser.positionalArguments();

    if (!pathList.isEmpty()) {
        for (auto &src : pathList) src = cleanPath(src);

        uint8_t overWrite = OW::Confirm;
        uint8_t procOptions = 0, mdlxOptions = 0;
        uint32_t optOptions = 0, sanOptions = 0;
        int precision = 6;

        procOptions |= mode;
        addOptions(procOptions, Proc::map);
        addOptions(mdlxOptions, Mdlx::map);

        if (parser.isSet("conflict")) {
            QString ow = parser.value("conflict").toUpper();

            if (ow == "OVERWRITE") overWrite = OW::YesAll;
            else if (ow == "SKIP") overWrite = OW::NoAll;
            else if (ow == "INCREMENT") overWrite = OW::IncrAll;
        }

        if (parser.isSet(Mdlx::version.first[2])) {
            QString version = parser.value(Mdlx::version.first[2]).toLower();

            if (Version::string2Flag.count(version) == 1) {
                mdlxOptions |= Version::string2Flag[version];
            }
        }

        if (mode & Proc::Convert) {
            addOption(procOptions, Proc::SuffixConv, Proc::suffixConv);
        }

        if (mode & Proc::Optimize) {
            addOption(procOptions, Proc::SuffixOpt, Proc::suffixOpt);

            if (parser.isSet(Opt::precision.first[2])) {
                if (parser.value(Opt::precision.first[2]).toUpper() == "OFF") {
                    precision = -1;
                }
                else {
                    precision = parser.value(Opt::precision.first[2]).toInt();
                    if (precision < 3 || precision > 10) precision = -1;
                }
            }

            addOptions(optOptions, Opt::SEQS::map);
            addOptions(optOptions, Opt::mtMap);
            addOptions(optOptions, Opt::GEOS::map);
            addOptions(optOptions, Opt::NODS::map);
            addOptions(optOptions, Opt::MISC::map);
            addOptions(sanOptions, San::WARN::map);
        }

        mop.process(*this, pathList, cleanPath(parser.value("destination")),
                    overWrite, procOptions, mdlxOptions, optOptions, sanOptions, precision);
    }
    else {
        showStatus("Please provide a source file.", Msg::Error, true);
    }
}

void Cmd::addParserOption(const QStringList &list)
{
    parser.addOption({ list[2], list[3], list[2] });
}

void Cmd::confirm(const QString &path, bool multiple)
{
    std::string answer;

    for (bool validInput=false; !validInput; validInput = confirmString2Int.count(answer) == 1) {
        showStatus("\nFile will be overwritten:\n" + path + "\nAre you sure? "
                   + (multiple ? "(Yes/YesAll/Increment/IncrementAll/No/NoAll)"
                               : "(Yes/Increment/No)")
                   + "\n", Msg::Notice, false);

        answer = std::string();
        std::cin >> answer;
    }

    emit confirmed(confirmString2Int[answer]);
}

void Cmd::showStatus(const QString &msg, uint8_t type, bool finished)
{
    QString msgnl = QString(msg).replace("<br />", "\n");
    if (previousType != type) msgnl.prepend('\n');

    switch(type) {
    case Msg::Success: case Msg::Notice:
        std::cout << msgnl.toStdString() << std::endl;
        break;
    case Msg::Error:
        std::cerr << msgnl.toStdString() << std::endl;
        break;
    }

    previousType = type;

    if (finished) {
        mop.quit();
        QCoreApplication::exit(type == Msg::Error);
    }
}
