#include "util.h"

#include <QDir>

QString cleanPath(const QString &path)
{
    return QDir::toNativeSeparators(QDir::cleanPath(path));
}
