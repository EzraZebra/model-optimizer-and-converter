#include "cmd.h"

#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Cmd c(Proc::Optimize);

    return a.exec();
}
