#include "options.h"

/** Proc */
    std::map<uint8_t, std::pair<QStringList, bool>> Proc::map = {
        { Recursive, {
              {
                  "Recursive Search", "Recursively search folders for model files.",
                  "recursive", "Recursively search folders for model files. [ON|OFF]\nDefault: ON"
              }, true } },
        { Mdx, {
              {
                  "Process MDX Files", "",
                  "mdx", "Process .mdx files. [ON|OFF]\nDefault: ON"
              }, true } },
        { Mdl, {
              {
                  "Process MDL Files", "",
                  "mdl", "Process .mdl files. [ON|OFF]\nDefault: ON",
              }, true } },
    };

    std::pair<QStringList, bool> Proc::suffixOpt = {
        {
            "_opt Suffix", "Add \"_opt\" to filenames.",
            "suffix", "Add \"_opt\" to filenames. [ON|OFF]\nDefault: ON",
        }, true
    };

    std::pair<QStringList, bool> Proc::suffixConv = {
        {
            "_conv Suffix", "Add \"_conv\" to filenames.",
            "suffix", "Add \"_conv\" to filenames. [ON|OFF]\nDefault: ON",
        }, false
    };

/** Mdlx */
    std::pair<QStringList, bool> Mdlx::version = {
        {
            "Version:", "Save the model as version (experimental).",
            "version", "Set the format version (experimental). [OFF|800|900|1000]\nDefault: OFF",
        }, false
    };

    std::map<uint8_t, std::pair<QStringList, bool>> Mdlx::map = {
        { mdlx::Flag::Magos, {
              {
                  "Magos Compatible", "Drops Global Sequence ID from Event Objects in MDL files, because Magos can't parse them.\nOnly applies to MDL files saved as v800 (classic).",
                  "magos", "Drops Global Sequence ID from Event Objects in MDL files [ON|OFF].\nDefault: ON",
              }, true } },
    };

    const QStringList Version::str = { "800", "900", "1000" };

    std::map<QString, uint8_t> Version::string2Flag = {
        { str[0], mdlx::Flag::v800 },
        { str[1], mdlx::Flag::v900 },
        { str[2], mdlx::Flag::v1000 },
    };

/** Opt */
    std::pair<QStringList, bool> Opt::precision = {
        {
            "Precision:", "Round floating point numbers to X decimals.\nNote: this has no direct effect on MDX filesize, but may improve results of other optimizations.",
            "precision", "Round floating point numbers to X decimals. [OFF|3-10]\nDefault: 6",
        }, true
    };

    std::map<uint32_t, std::pair<QStringList, bool>> Opt::SEQS::map = {
        { Linearize, {
              {
                  "Linearize Animations", "Change bezier and hermite animations to linear interpolation.",
                  "linearize", "Change bezier and hermite animations to linear interpolation (partial). [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { DeleteEmpty, {
              {
                  "Delete Empty Animations", "Delete animations without any keyframes.",
                  "emptyanims", "Delete animations without any keyframes. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { DeleteUnused, {
              {
                  "Delete Unused Keyframes", "Delete keyframes that aren't part of any sequences.",
                  "unusedframes", "Delete keyframes that aren't part of any sequences. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { DeleteSameish, {
              {
                  "Delete Same-ish Keyframes", "Delete consecutive keyframes that have about the same value.",
                  "sameishframes", "Delete consecutive keyframes that have about the same value. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { ZeroLength, {
              {
                  "Delete 0-length Sequences", "Delete sequences with a length of 0 or less.",
                  "zerolength", "Delete sequences with a length of 0 or less. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { ZeroLengthGlob, {
              {
                  "Delete 0-length Global Sequences", "Delete global sequences with a length of 0.",
                  "zerolengthglob", "Delete global sequences with a length of 0. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
    };

    std::map<uint32_t, std::pair<QStringList, bool>> Opt::mtMap = {
        { MTLS::Delete, {
              {
                  "Delete Unused Materials", "",
                  "unusedmtls", "Delete unused materials. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { MTLS::Merge, {
              {
                  "Merge Identical Materials", "",
                  "identicalmtls", "Merge identical materials. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { TEXS::Delete, {
              {
                  "Delete Unused Textures", "",
                  "unusedtexs", "Delete unused textures. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { TEXS::Merge, {
              {
                  "Merge Identical Textures", "",
                  "identicaltexs", "Merge identical textures. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { TXAN::Delete, {
              {
                  "Delete Unused Texture Animations", "",
                  "unusedtxan", "Delete unused texture animations. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { TXAN::Merge, {
              {
                  "Merge Identical Texture Animations", "",
                  "identicaltxan", "Merge identical texture animations. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
    };

    std::map<uint32_t, std::pair<QStringList, bool>> Opt::GEOS::map = {
        { VtxDelete, {
              {
                  "Delete Free Vertices", "Delete vertices not part of any triangle.\nIf no vertices are left, the geoset is deleted.",
                  "freevtcs", "Delete free vertices. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { VtxMerge, {
              {
                  "Merge Identical Vertices", "Merge vertices with the same coordinates, UV coordinates, vertex group, tangents, and skin weights.",
                  "identicalvtcs", "Merge identical vertices. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { PreserveNrms, {
              {
                  "Preserve Normals", "Preserve normals when merging vertices.\nIf unchecked, normals are averaged.",
                  "preservenrms", "Preserve normals when merging vertices. [ON|OFF]\nDefault: OFF",
              }, false
          }
        },
        { Tris, {
              {
                  "Delete Fake Triangles", "Delete triangles where two or more vertices are the same.",
                  "faketris", "Delete fake triangles. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { MtxGrpDelete, {
              {
                  "Delete Unused Matrix Groups", "Delete matrix groups not used by any vertices.",
                  "unusedmtxgrps", "Delete unused matrix groups. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { SeqExt, {
              {
                  "Delete Excess Sequence Extents", "Delete sequence extents to match the number of sequences.",
                  "excessseqexts", "Delete sequence extents to match the number of sequences. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { Merge, {
              {
                  "Merge Similar Geosets", "Merge geosets with an identical geoset animation and the same material, selection group, selection flags, LOD, and LOD name.\nExtents and sequence extents will take the largest values.\nUse with caution.",
                  "similargeos", "Merge similar geosets. Use with caution. [ON|OFF]\nDefault: OFF",
              }, false
          }
        },
        { GeoAnimDelete, {
              {
                  "Delete Unused Geoset Animations", "Delete geoset animations that don't refer to a geoset and aren't referred to by a bone.",
                  "unusedgeoa", "Delete unused geoset animations. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { GeoAnimDuplDelete, {
              {
                  "Delete Duplicate Geoset Animations", "Delete extra geoset animations that refer to the same geoset.\nIf it's referred to by any bones, the geoset will be set to none instead.",
                  "duplicategeoa", "Delete extra geoset animations that refer to the same geoset. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
    };

    std::map<uint32_t, std::pair<QStringList, bool>> Opt::NODS::map = {
        { BoneHelperDelete, {
              {
                  "Delete Unused Bones and Helpers", "Delete bones and helpers without children and not connected to any geosets.",
                  "unusedbones", "Delete unused bones and helpers. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { EventObjDelete, {
              {
                  "Delete Unused Event Objects", "Delete event objects without any tracks.",
                  "unusedevts", "Delete unused event objects. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
    };

    std::map<uint32_t, std::pair<QStringList, bool>> Opt::MISC::map = {
        { UnknownChunksDelete, {
              {
                  "Delete Unknown Chunks", "Delete unknown chunks from MDX.\nThese are sometimes added by model editors.",
                  "unknownchunks", "Delete unknown chunks from MDX. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { BindPose, {
              {
                  "Delete Excess Bind Pose Matrices", "Delete matrices from bind pose to match the number of nodes.",
                  "excessbindpose", "Delete excess bind pose matrices. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
    };

/** San */
    std::map<uint32_t, std::pair<QStringList, bool>> San::WARN::map = {
        { SeqExts, {
              {
                  "Add Missing Sequence Extents", "Add sequence extents to geosets to match the number of sequences.\nThe last sequence extent or the geoset extent will be copied if available.",
                  "missingseqexts", "Add missing sequence extents. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { LightAtt, {
              {
                  "Clamp Light Attenuation", "Set light attenuation minimum to 80 if less, and maximum to 200 if more.",
                  "clamplight", "Clamp light attentuation to minimum 80 and maximum 200. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
        { BindPose, {
              {
                  "Add Missing Bind Pose Matrices", "Add matrices to bind pose to match the number of nodes.\nThe last bind pose matrix will be copied if available.",
                  "missingbindpose", "Add missing bind pose matrices. [ON|OFF]\nDefault: ON",
              }, true
          }
        },
    };
