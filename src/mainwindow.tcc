#ifndef MAINWINDOW_TCC
#define MAINWINDOW_TCC

#include "mainwindow.h"
#include "options.h"

#include <QGroupBox>

#include <cstring>

template<class L>
L* MainWindow::makeLayout(QWidget *tab)
{
    auto *layout = new L;

    if (tab == nullptr) centralLayout.addLayout(layout);
    else tab->setLayout(layout);

    layout->setAlignment(Qt::AlignLeft|Qt::AlignTop);

    return layout;
}

template<class L, class P>
L* MainWindow::makeLayout(P *parent)
{
    auto *layout = new L;

    if (parent == nullptr) centralLayout.addLayout(layout);
    else parent->addLayout(layout);

    layout->setAlignment(Qt::AlignLeft|Qt::AlignTop);

    return layout;
}

template<class L>
QGridLayout* MainWindow::makeGroupBox(L *layout, const QString &title)
{
    auto *groupBox = new QGroupBox(title);
    groupBox->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum));
    layout->addWidget(groupBox);

    auto *gridLayout = new QGridLayout;
    groupBox->setLayout(gridLayout);

    return gridLayout;
}

template<class C, class L, typename T>
C* MainWindow::addCheckBox(L *layout, const QString &title, const char *prop, T propVal,
                           bool defaultOn, const QString &tooltip, int x, int y)
{
    auto *checkBox = new C(title);
    QVariant setVal = settings.value(getSettingStr(prop, title, propVal));
    x=x+0; y=y+0; //silence not used warning

    checkBox->setProperty(prop, propVal);
    checkBox->setChecked((defaultOn && setVal.toByteArray().isEmpty()) || setVal.toBool());
    checkBox->setToolTip(tooltip);

    if constexpr (std::is_same<C, QCheckBox>()) {
        checkBoxes[prop].emplace_back(checkBox);
    }
    else if constexpr (std::is_same<C, QRadioButton>()) {
        radioButtons[prop].emplace_back(checkBox);
    }

    if constexpr (std::is_same<L, QGridLayout>()) {
        layout->addWidget(checkBox, x, y);
    }
    else layout->addWidget(checkBox);

    return checkBox;
}

template<class L, typename T>
QCheckBox* MainWindow::addCheckBox(L *layout, const char *prop, T propVal, std::map<T, std::pair<QStringList, bool>> &map)
{
    return addCheckBox<QCheckBox>(layout, map.at(propVal).first[0], prop, propVal, map[propVal].second, map[propVal].first[1]);
}

template<class L, typename T>
QCheckBox* MainWindow::addCheckBox(L *layout, const char *prop, T propVal, const std::pair<QStringList, bool> &pair)
{
    return addCheckBox<QCheckBox>(layout, pair.first[0], prop, propVal, pair.second, pair.first[1]);
}

template<typename T>
void MainWindow::addSubTab(QTabWidget *tab, const QString &title, const char *prop, const std::map<T, std::pair<QStringList, bool>> &map)
{
    auto *subTab = new QWidget;
    auto *gridLayout = makeLayout<QGridLayout>(subTab);
    tab->addTab(subTab, title);

    size_t cols = map.size() / 3;
    size_t row=0, column=0;
    for (const auto &option : map) {
        auto *checkBox = addCheckBox<QCheckBox>(gridLayout, option.second.first[0], prop, option.first, option.second.second,
                                                option.second.first[1], row, column);

        if (++column >= cols) {
            column = 0;
            row++;
        }

        if (option.first == Opt::GEOS::VtxMerge) {
            vtxMergeCheck = checkBox;
        }
        else if (option.first == Opt::GEOS::PreserveNrms) {
            preserveNrmsCheck = checkBox;
        }
        else if (option.first == Opt::MISC::UnknownChunksDelete) {
            unknownChunksCheck = checkBox;
        }
    }
}

#endif // MAINWINDOW_TCC
