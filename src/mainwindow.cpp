#include "mainwindow.h"
#include "options.h"

#include <QLabel>
#include <QCheckBox>
#include <QRadioButton>
#include <QFileDialog>
#include <QMimeData>
#include <QDragEnterEvent>

std::unordered_map<QMessageBox::StandardButton, uint8_t> MainWindow::confirmBtn2Int = {
    { QMessageBox::Yes, OW::Yes },
    { QMessageBox::YesToAll, OW::YesAll },
    { QMessageBox::Save, OW::Incr },
    { QMessageBox::SaveAll, OW::IncrAll },
    { QMessageBox::No, OW::No },
    { QMessageBox::NoToAll, OW::NoAll },
};

QString MainWindow::getSettingStr(const char *prop, const QString &title, int propVal) const
{
    return prop + title.simplified().replace(" ", QString()) + QString::number(propVal);
}

MainWindow::MainWindow()
{
    setAcceptDrops(true);
    setWindowTitle("MOP");

    QFile style("style.css");
    if (style.open(QIODevice::ReadOnly | QIODevice::Text)) {
        setStyleSheet(style.readAll());
    }
    else statusText.show("oO");

    // Get saved Path List
    pathList = settings.value("PathList").value<QStringList>();

    // Restore geometry
    if (!settings.value("geometry").toByteArray().isEmpty()) {
        restoreGeometry(settings.value("geometry").toByteArray());
    }

    setCentralWidget(new QWidget);
    centralWidget()->setLayout(&centralLayout);

    // Source path
        pathEdit.setText(settings.value("Path").toString());
        connect(&pathEdit, &QLineEdit::textEdited, this, &MainWindow::pathEdited);
        centralLayout.addWidget(&pathEdit);

        auto *hLayout = makeLayout<QHBoxLayout>();

        auto *pushButton = new QPushButton("Select Models");
        hLayout->addWidget(pushButton);
        connect(pushButton, &QPushButton::clicked, this, &MainWindow::browseModels);

        pushButton = new QPushButton("Select Folder");
        hLayout->addWidget(pushButton);
        connect(pushButton, &QPushButton::clicked, this, &MainWindow::browseFolder);

        addCheckBox(hLayout, "proc", Proc::Recursive, Proc::map);

    // Target Path
        targetEdit.setText(settings.value("TargetPath").toString());
        targetEdit.setPlaceholderText("Leave blank to keep source folder and filename");
        centralLayout.addWidget(&targetEdit);

        hLayout = makeLayout<QHBoxLayout>();

        pushButton = new QPushButton("Select Folder");
        hLayout->addWidget(pushButton);
        connect(pushButton, &QPushButton::clicked, this, &MainWindow::browseTarget);

        auto *label = new QLabel("Conflicts:");
        hLayout->addWidget(label);

        addCheckBox<QRadioButton>(hLayout, "Ask", "ow", OW::Confirm, true, "Ask for confirmation before overwriting files.");
        addCheckBox<QRadioButton>(hLayout, "Overwrite", "ow", OW::YesAll, false, "Overwrite files without asking.");
        addCheckBox<QRadioButton>(hLayout, "Skip", "ow", OW::NoAll, false, "Skip existing files without asking.");
        addCheckBox<QRadioButton>(hLayout, "Increment", "ow", OW::IncrAll, false, "Increment filenames with a number.");

    // Divider
    auto *divider = new QFrame;
    divider->setFrameShape(QFrame::HLine);
    divider->setFrameShadow(QFrame::Sunken);
    centralLayout.addWidget(divider);

    // Main Options
    hLayout = makeLayout<QHBoxLayout>();

        optimizeCheck   = addCheckBox<QCheckBox>(hLayout, "Optimize", "proc", Proc::Optimize, true);
        sanitizeCheck   = addCheckBox<QCheckBox>(hLayout, "Sanitize", "proc", Proc::Sanitize, true);
        convertCheck    = addCheckBox<QCheckBox>(hLayout, "Convert", "proc", Proc::Convert, true);
        mdxCheck        = addCheckBox(hLayout, "proc", Proc::Mdx, Proc::map);
        mdlCheck        = addCheckBox(hLayout, "proc", Proc::Mdl, Proc::map);

        connect(optimizeCheck, &QCheckBox::toggled, this, &MainWindow::boxChecked);
        connect(sanitizeCheck, &QCheckBox::toggled, this, &MainWindow::boxChecked);
        connect(convertCheck, &QCheckBox::toggled, this, &MainWindow::boxChecked);
        connect(mdxCheck, &QCheckBox::toggled, this, &MainWindow::boxChecked);
        connect(mdlCheck, &QCheckBox::toggled, this, &MainWindow::boxChecked);

    // Tabs
    tabs.setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum));
    centralLayout.addWidget(&tabs);

    // Output Tab
        auto *tab = new QWidget;
        auto *vLayout = makeLayout<QVBoxLayout>(tab);
        tabs.addTab(tab, "Output");

        hLayout = makeLayout<QHBoxLayout>(vLayout);
        suffixOptCheck  = addCheckBox(hLayout, "proc", Proc::SuffixOpt, Proc::suffixOpt);
        suffixConvCheck = addCheckBox(hLayout, "proc", Proc::SuffixConv, Proc::suffixConv);

        hLayout = makeLayout<QHBoxLayout>(vLayout);
        versionCheck = addCheckBox(hLayout, "mdlx", Mdlx::Version, Mdlx::version);
        connect(versionCheck, &QCheckBox::toggled, this, &MainWindow::boxChecked);

        versionSelect.addItems(Version::str);
        versionSelect.setCurrentText(settings.value("VersionSelect").toString());
        versionSelect.setToolTip("800: Classic\n900: Reforged (obsolete)\n1000: Reforged");
        connect(&versionSelect, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &MainWindow::boxChecked);
        hLayout->addWidget(&versionSelect);

        magosCheck = addCheckBox(vLayout, "mdlx", mdlx::Flag::Magos, Mdlx::map);

    // Optimize Tab
        vLayout = makeLayout<QVBoxLayout>(&optimizeTab);
        tabs.addTab(&optimizeTab, "Optimization");

        hLayout = makeLayout<QHBoxLayout>(vLayout);

            addCheckBox(hLayout, "opt", Opt::Precision, Opt::precision);
            precisionSelect.addItems({ "3", "4", "5", "6", "7", "8", "9", "10" });
            precisionSelect.setCurrentText(settings.value("PrecisionSelect").toString());
            hLayout->addWidget(&precisionSelect);

        optTabs = new QTabWidget;
        vLayout->addWidget(optTabs);
            addSubTab(optTabs, "Sequences && Animations", "opt", Opt::SEQS::map);
            addSubTab(optTabs, "Materials && Textures", "opt", Opt::mtMap);
            addSubTab(optTabs, "Geosets", "opt", Opt::GEOS::map);
            addSubTab(optTabs, "Nodes", "opt", Opt::NODS::map);
            addSubTab(optTabs, "Other", "opt", Opt::MISC::map);

            connect(vtxMergeCheck, &QCheckBox::toggled, this, &MainWindow::boxChecked);

    // Sanitize Tab
        vLayout = makeLayout<QVBoxLayout>(&sanitizeTab);
        tabs.addTab(&sanitizeTab, "Sanitization");

        sanTabs = new QTabWidget;
        vLayout->addWidget(sanTabs);
            addSubTab(sanTabs, "Warnings", "san", San::WARN::map);


    // Open tab
    tabs.setCurrentIndex(settings.value("ActiveTab").toInt());
    optTabs->setCurrentIndex(settings.value("ActiveTabOpt").toInt());
    boxChecked(true);

    // Process Button
    connect(&processButton, &QPushButton::clicked, this, &MainWindow::processClicked);
    centralLayout.addWidget(&processButton);

    // Status
    centralLayout.addWidget(&statusText);

    uiReady = true;
}

MainWindow::~MainWindow()
{
    // If the UI has been setup, save settings
    if (uiReady) {
        settings.setValue("Geometry", saveGeometry());
        settings.setValue("Path", pathEdit.text());
        settings.setValue("TargetPath", targetEdit.text());
        settings.setValue("PathList", QVariant::fromValue(pathList));
        settings.setValue("ActiveTab", tabs.currentIndex());
        settings.setValue("ActiveTabOpt", optTabs->currentIndex());

        for (const auto &set : checkBoxes) for (const auto &checkBox : set.second) {
            settings.setValue(getSettingStr(set.first, checkBox->text(), checkBox->property(set.first).toInt()), checkBox->isChecked());
        }

        settings.setValue("VersionSelect", versionSelect.currentText());
        settings.setValue("PrecisionSelect", precisionSelect.currentText());

        for (const auto &set : radioButtons) for (const auto &radioButton : set.second) {
            settings.setValue(getSettingStr(set.first, radioButton->text(), radioButton->property(set.first).toInt()), radioButton->isChecked());
        }
    }
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
    centralWidget()->setDisabled(true);
}

void MainWindow::dragLeaveEvent(QDragLeaveEvent*)
{
    centralWidget()->setDisabled(false);
}

void MainWindow::dropEvent(QDropEvent *event)
{
    if (event->mimeData()->hasUrls()) {
        event->acceptProposedAction();

        QStringList paths;

        const QList<QUrl> urls = event->mimeData()->urls();
        for (const auto &url : urls) {
            paths.append(url.toLocalFile());
        }

        doProcess(paths);
    }

    centralWidget()->setDisabled(false);
}

void MainWindow::pathEdited()
{
    pathList = QStringList(pathEdit.text());
}

void MainWindow::browseFolder()
{
    QString path = QFileDialog::getExistingDirectory(this, "Select Source Folder", pathEdit.text(),
                                                     QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if (!path.isEmpty()) {
        path = cleanPath(QDir().absoluteFilePath(path));
        pathList = QStringList(path);
        pathEdit.setText(path);
    }
}

void MainWindow::browseModels()
{
    QStringList newPathList = QFileDialog::getOpenFileNames(this, "Select Models", pathEdit.text(),
                                                            "Models (*mdx *.mdl)", nullptr, QFileDialog::DontResolveSymlinks);

    if (!newPathList.isEmpty()) {
        QString path;

        for (auto &p : newPathList) {
            p = cleanPath(QDir().absoluteFilePath(p));

            // Get the shortest path
            if (newPathList.size() > 1) {
                QString newPath = cleanPath(QFileInfo(p).absolutePath());
                if (!newPath.isEmpty() && (newPath.length() < path.length() || path.isEmpty())) {
                    path = newPath;
                }
            }
        }

        // Put the appropriate path in pathEdit
        if (newPathList.size() == 1) path = newPathList.first();

        if (!path.isEmpty()) {
            if (newPathList.size() > 1) {
                if (!path.endsWith(QDir::separator())) {
                    path.append(QDir::separator());
                }
                path.append("(multiple)");
            }

            pathEdit.setText(path);
        }

        pathList = newPathList;
    }
}

void MainWindow::browseTarget()
{
    QString path = QFileDialog::getExistingDirectory(this, "Select Target Folder", targetEdit.text(),
                                                     QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if (!path.isEmpty()) targetEdit.setText(cleanPath(path));
}

void MainWindow::boxChecked(bool)
{
    bool conv = convertCheck->isChecked();
    bool opt = optimizeCheck->isChecked();
    bool mdx = mdxCheck->isChecked();

    suffixOptCheck->setEnabled(opt);
    suffixConvCheck->setEnabled(conv);
    magosCheck->setEnabled(((conv && mdx) || (!conv && mdlCheck->isChecked()))
                           && (!versionCheck->isChecked() || versionSelect.currentText() == "800"));

    preserveNrmsCheck->setEnabled(vtxMergeCheck->isChecked());
    unknownChunksCheck->setEnabled(mdx);
    optimizeTab.setEnabled(opt);

    sanitizeTab.setEnabled(sanitizeCheck->isChecked());
}

void MainWindow::doProcess(const QStringList &paths)
{
    processButton.setDisabled(true);

    int overWrite = OW::Confirm, procOptions = 0, mdlxOptions = 0, optOptions = 0, sanOptions = 0;
    int precision = -1;

    for (const auto &checkBox : checkBoxes["proc"]) {
        if (checkBox->isChecked() && checkBox->isEnabled()) {
            procOptions |= checkBox->property("proc").toInt();
        }
    }

    for (const auto &radioButton : radioButtons["ow"]) {
        if (radioButton->isChecked() && radioButton->isEnabled()) {
            overWrite = radioButton->property("ow").toInt();
            break;
        }
    }

    for (const auto &checkBox : checkBoxes["mdlx"]) {
        if (checkBox->isChecked() && checkBox->isEnabled()) {
            int prop = checkBox->property("mdlx").toInt();

            switch(prop)
            {
            case Mdlx::Version: mdlxOptions |= Version::string2Flag[versionSelect.currentText()];
                break;
            default: mdlxOptions |= prop;
                break;
            }
        }
    }

    if (procOptions & Proc::Optimize) {
        for (const auto &checkBox : checkBoxes["opt"]) {
            if (checkBox->isChecked() && checkBox->isEnabled()) {
                int prop = checkBox->property("opt").toInt();

                optOptions |= prop;
                if (prop == Opt::Precision) precision = precisionSelect.currentText().toInt();
            }
        }
    }

    if (procOptions & Proc::Sanitize) {
        for (const auto &checkBox : checkBoxes["san"]) {
            if (checkBox->isChecked() && checkBox->isEnabled()) {
                sanOptions |= checkBox->property("san").toInt();
            }
        }
    }

    mop.process(*this, paths, targetEdit.text(), overWrite, procOptions, mdlxOptions, optOptions, sanOptions, precision);
}

void MainWindow::processClicked()
{
    doProcess(pathList);
}

void MainWindow::confirm(const QString &path, bool multiple)
{
    QMessageBox confirm(this);

    confirm.addButton(QMessageBox::Yes);
    auto *btn = confirm.addButton(QMessageBox::Save);
    btn->setText("Increment");
    confirm.addButton(QMessageBox::No);

    if (multiple) {
        confirm.addButton(QMessageBox::YesToAll);
        btn = confirm.addButton(QMessageBox::SaveAll);
        btn->setText("Increment All");
        confirm.addButton(QMessageBox::NoToAll);
    }
    confirm.setText("File will be overwritten:<br />" + path + "<br />Are you sure?");
    confirm.exec();

    QMessageBox::StandardButton answer = confirm.standardButton(confirm.clickedButton());

    emit confirmed(confirmBtn2Int.count(answer) == 0 ? OW::Confirm : confirmBtn2Int[answer]);
}

void MainWindow::showStatus(const QString &msg, uint8_t type, bool finished)
{
    statusText.show(msg, type);

    if (finished) {
        mop.quit();
        statusText.separator();
        processButton.setDisabled(false);
    }
}
