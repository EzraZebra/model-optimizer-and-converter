#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "mop.h"
#include "statustext.h"

#include <QMainWindow>
#include <QMessageBox>
#include <QSettings>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>

/** Forward declare */
class QRadioButton;

/** @brief The main GUI application. */
class MainWindow : public QMainWindow
{
    Q_OBJECT

    static std::unordered_map<QMessageBox::StandardButton, uint8_t> confirmBtn2Int;

    Mop         mop;
    QSettings   settings = QSettings("EzraZebra", "MDLX Converter");
    QStringList pathList;
    bool        uiReady = false;

    QVBoxLayout centralLayout;
    QLineEdit   pathEdit, targetEdit;
    QTabWidget  tabs;
    QTabWidget  *optTabs, *sanTabs;
    QWidget     optimizeTab, sanitizeTab;
    QComboBox   versionSelect, precisionSelect;
    QPushButton processButton = QPushButton("Process");
    StatusText  statusText;

    QCheckBox *optimizeCheck, *sanitizeCheck, *convertCheck, *mdxCheck, *mdlCheck;
    QCheckBox *suffixOptCheck, *suffixConvCheck;
    QCheckBox *versionCheck, *magosCheck;
    QCheckBox *unknownChunksCheck;
    QCheckBox *vtxMergeCheck, *preserveNrmsCheck;

    std::map<const char*, std::vector<QCheckBox*>>     checkBoxes;
    std::map<const char*, std::vector<QRadioButton*>>  radioButtons;

    QString getSettingStr(const char *prop, const QString &title, int propVal) const;

    template<class L>
    L* makeLayout(QWidget *tab = nullptr);
    template<class L, class P>
    L* makeLayout(P *parent);

    template<class L>
    QGridLayout* makeGroupBox(L *layout, const QString &title = QString());

    template<class C, class L, typename T>
    C* addCheckBox(L *layout, const QString &title, const char *prop, T propVal,
                   bool defaultOn, const QString &tooltip = QString(),
                   int x=0, int y=0);

    template<class L, typename T>
    QCheckBox* addCheckBox(L *layout, const char *prop, T propVal, std::map<T, std::pair<QStringList, bool>> &map);
    template<class L, typename T>
    QCheckBox* addCheckBox(L *layout, const char *prop, T propVal, const std::pair<QStringList, bool> &pair);

    template<typename T>
    void addSubTab(QTabWidget *tab, const QString &title, const char *prop, const std::map<T, std::pair<QStringList, bool>> &map);

    void dragEnterEvent(QDragEnterEvent *event) override;
    void dragLeaveEvent(QDragLeaveEvent*) override;
    void dropEvent(QDropEvent *event) override;

    /** @brief Convenience function that emit the convert signal with all parameters filled. */
    void doProcess(const QStringList &paths);

    void processPathList(QStringList &list);

public:
    MainWindow();
    ~MainWindow();

private slots:
    void pathEdited();
    void browseFolder();
    void browseModels();
    void browseTarget();
    void boxChecked(bool);

    /** @brief Receives signal from Convert button. */
    void processClicked();

public slots:
    /** @brief Ask the user for overwrite confirmation and send the answer to the converter */
    void confirm(const QString &path, bool multiple);
    /** @brief Output a message and enable Convert button if finished */
    void showStatus(const QString &msg, uint8_t type, bool finished=false);

signals:
    void confirmed(uint8_t btn);
    void processFinished();
};

#include "mainwindow.tcc"

#endif // MAINWINDOW_H
