#ifndef MOP_TCC
#define MOP_TCC

#include "mop.h"
#include "processor.h"

template<class APP>
void Mop::process(APP &app, const QStringList &paths, const QString &target,
                  uint8_t owOptions, uint8_t procOptions, uint8_t mdlxOptions,
                  uint32_t optOptions, uint32_t sanOptions, int precision)
{
    Processor *processor = new Processor(paths, target, owOptions, procOptions, mdlxOptions, optOptions, sanOptions, precision);
    processor->moveToThread(&thread);

    connect(this, &Mop::startProcess, processor, &Processor::process);
    connect(&thread, &QThread::finished, processor, &QObject::deleteLater);
    connect(processor, &Processor::status, &app, &APP::showStatus);
    connect(processor, &Processor::confirm, &app, &APP::confirm);
    connect(&app, &APP::confirmed, processor, &Processor::confirmed);
    connect(&app, &APP::processFinished, this, &Mop::quit);

    thread.start();
    emit startProcess();
}

#endif // MOP_TCC
