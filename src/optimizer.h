#ifndef OPTIMIZER_H
#define OPTIMIZER_H

#include "util.h"

#include <model_edit.h>

#include <QObject>

#include <memory>

/** Forward declare */
namespace mdlx
{
class Model;
class AnimEditBase;
template<typename T>
class AnimationEdit;
class MaterialEdit;
class GeosetEdit;
class MatrixGroup;
}

class Optimizer : public QObject
{
    Q_OBJECT

    uint32_t diffCount = 0, options = 0, sanOptions = 0;
    int precision = -1;

    mdlx::Model &model;

    void round(float &val);
    template<size_t S>
    void round(std::array<float, S> &arr);
    template<size_t S>
    std::array<float, S> round(const std::array<float, S> &arr);
    template<size_t S, size_t Z>
    void round(std::array<std::array<float, S>, Z> &arr);
    template<typename T>
    void round(std::vector<T> &vect);

    // Rounding
    void doRounding(const mdlx::Animations &animations);

    // Sequences & Animations
    void deleteZeroLengthSequences();
    void deleteZeroLengthGlobalSequences();
    void linearizeAnimations(const mdlx::Animations &animations);
    template<typename T>
    void checkKeyframeUnused(mdlx::AnimationEdit<T> *animation);
    void deleteUnusedKeyframes(const mdlx::Animations &animations);
    template<typename T>
    void checkKeyframeSameish(mdlx::AnimationEdit<T> *animation);
    void deleteSameishKeyframes(const mdlx::Animations &animations);
    void checkAnimationsEmpty(mdlx::Animations &animations);
    void deleteEmptyAnimations(const mdlx::AnimatedObjects &animatedObjects);

    // Materials & Textures
    void deleteUnusedMaterials();
    void mergeIdenticalMaterials();
    void deleteUnusedTextures();
    void mergeIdenticalTextures();
    void deleteUnusedTextureAnimations();
    void mergeIdenticalTextureAnimations();

    // Geosets
    void deleteFakeTriangles();
    void deleteFreeVertices();
    void mergeIdenticalVertices();
    void deleteUnusedMatrixGroups();
    void deleteSequenceExtents();
    void mergeSimilarGeosets();
    void deleteUnusedGeosetAnimations();
    void deleteDuplicateGeosetAnimations();

    // Nodes
    void deleteUnusedBonesHelpers();
    void deleteUnusedEventObjects();

    // Misc
    void deleteBindPoseMatrices();

    // Sanitize
    // Warnings
    void addSequenceExtents();
    void clampLightAttenuation();
    void addBindPoseMatrices();

public:
    Optimizer(mdlx::Model &model, uint32_t options, uint32_t sanOptions, int precision);
    void optimize();

signals:
    void status(const QString &msg, uint8_t type = Msg::Notice, bool finished = false);
};

#include "optimizer.tcc"

#endif // OPTIMIZER_H
