#ifndef OPTIMIZER_TCC
#define OPTIMIZER_TCC

#include "optimizer.h"

template<size_t S>
void Optimizer::round(std::array<float, S> &arr)
{
    if (precision >= 0) for (auto &val : arr) round(val);
}

template<size_t S>
std::array<float, S> Optimizer::round(const std::array<float, S> &arr) {
    std::array<float, S> newArr = arr;
    round(newArr);
    return newArr;
}

template<size_t S, size_t Z>
void Optimizer::round(std::array<std::array<float, S>, Z> &arr)
{
    if (precision >= 0) for (auto &subarr : arr) round(subarr);
}

template<typename T>
void Optimizer::round(std::vector<T> &vect)
{
    for (auto &el : vect) round(el);
}

#endif // OPTIMIZER_TCC
