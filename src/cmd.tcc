#ifndef CMD_TCC
#define CMD_TCC

#include "cmd.h"

template <typename T>
void Cmd::addParserOptions(const std::map<T, std::pair<QStringList, bool>> &map)
{
    for (const auto &option : map) {
        addParserOption(option.second.first);
    }
}

template<typename T>
void Cmd::addOption(T &options, T id, const std::pair<QStringList, bool> &pair) const
{
    bool isSet = parser.isSet(pair.first[2]);
    bool on = isSet && parser.value(pair.first[2]).toUpper() == "ON";

    if ((pair.second && (!isSet || on)) || (!pair.second && on)) {
        options |= id;
    }
}

template<typename T>
void Cmd::addOptions(T &options, const std::map<T, std::pair<QStringList, bool>> &map) const
{
    for (const auto &option : map) {
        addOption(options, option.first, option.second);
    }
}

#endif // CMD_TCC
