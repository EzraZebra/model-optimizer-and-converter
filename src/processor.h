#ifndef PROCESSOR_H
#define PROCESSOR_H

#include "util.h"
#include "options.h"

#include <QObject>

/** Forward declare */
namespace mdlx
{
class Model;
}

/** Converter to be moved to a QThread. */
class Processor : public QObject
{
    Q_OBJECT

    QStringList pathList;
    QString     targetPath;
    uint8_t     overWrite = OW::Confirm;
    uint8_t     procOptions = 0;
    uint8_t     mdlxOptions = 0;
    uint32_t    optOptions = 0, sanOptions = 0;
    int         precision = -1;

    uint32_t successCount = 0, skipCount = 0, failCount = 0;
    size_t totalSize = 0, totalSizeOpt = 0;

    bool success(mdlx::Model &model);
    QString sizeDiff(size_t orig, size_t opt) const;

public:
    Processor(const QStringList &pathList, const QString &targetPath,
              uint8_t owOptions, uint8_t procOptions, uint8_t mdlxOptions,
              uint32_t optOptions, uint32_t sanOptions, int precision);

public slots:
    /** @brief Convert all files in pathList according to provided options. */
    void process();

    /** @brief Save received confirmation and continue conversion. */
    void confirmed(uint8_t btn);

signals:
    void status(const QString &msg, uint8_t type = Msg::Notice, bool finished = false);
    void confirm(const QString &path, bool multiple);
};

#endif // PROCESSOR_H
